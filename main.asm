;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;
;
;
;Ej 03
; stack en 0x01ffb000
; salta a ejecutarse en 0x0
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; debe calzar justo en 0xf0000, recordar que el BOCHS
; carga las imagenes de rom desde la ultima posicion hacia abajo
; asi que todas la etiquetas deben referenciase desde el final de la
; ROM

ROM_LOCATION	equ 	0xf0000				; direccion en el mapa de mem de ROM
%define 		CR0_PE	0x00000001 			; Protection Enable
%define 		CR0_pe	0xfffffffe 			; Protection disable
%define			copia1	0x00000000			; direccion de 1er copia segun enunciado
%define			copia2	0x00300000			; direccion de 2da copia segun enuncuado
%define			copia3	0x00800000			; direccion de 2da copia segun enuncuado
%define			pila	0x01ffb000			; direccion donde debe ubicarse la pila segun enunciado
initMark equ $





Inicio:
	;uso el stack como esta, despues lo muevo
;	call	HabilitarA20	;verifica y habilita gate A20
	xchg 	bx, bx	;bp

	mov		ax, ROM_LOCATION >> 4	; Segmento ubicado al comienzo del bloque
	mov		ds, ax
	lgdt 	[ds:gdtr_img]			; GDTR provisoria para 16 bits
	smsw  	eax
	or		eax, CR0_PE				; Establecer el uP en MP
	lmsw  	ax

	jmp		CS_SEL:dword (ROM_LOCATION + flush_prefetch_queue)
flush_prefetch_queue equ $






;;;;;;;;;;;;;;;;;;
; a partir de aca todo funciona en 32 bit, parte en modo prot. y parte en modo real
;;;;;;;;;;;;;;;;;;
USE32
	mov		ax, DS_SEL		;cargo selectores de segmentos de datos y stack en MP
	mov		ds, ax
	mov		es, ax
	mov		ss, ax
;lo siguiente es opcional y anda indistintamente en modo real o en MP
	smsw  	eax
	and		eax, CR0_pe				; Establecer el uP en modo real
	lmsw  	ax
	jmp		VUELTA_REAL



VUELTA_REAL:
	;inicializo un stack para no pisar el primer segmento de 64k con la copia
	; que pide el enunciado
	mov		esp, pila
;ahora copio por priemra vez
	mov 	esi, (ROM_LOCATION+initMark)
	mov 	edi, copia1
	mov 	ecx, Fin_codigo
	xchg 	bx, bx	;bp
	call	td3_memcopy
;ahora copio por segunda vez
	mov 	esi, (ROM_LOCATION+initMark)
	mov 	edi, copia2
	mov 	ecx, Fin_codigo
	xchg 	bx, bx	;bp
	call	td3_memcopy
;ahora copio por tercera vez
	mov 	esi, (ROM_LOCATION+initMark)
	mov 	edi, copia3
	mov 	ecx, Fin_codigo
	xchg 	bx, bx	;bp
	call	td3_memcopy
	
	jmp		LOOP_PRINCIPAL



LOOP_PRINCIPAL:
	hlt
	jmp		$	;por las dudas que salga del hlt
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;	RUTINA DE COPIA
;	los parametros para copia son los sigientes
;	y deben venir precargados en estos registros antes de invocar la funcion:
;
;	ds:si = puntero a segmento origen
;	es:di = puntero a segmento destino
;
;	cx = cantidad de bytes a copiar
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


td3_memcopy:
	push	eax
	push	ecx



Copia:
		lodsb
		stosb
	loop 	Copia ;(decrementa cx e incrementa si,di acorde al tamanio de los datos a mover)
	xchg 	bx, bx	;bp
	pop		ecx
	pop		eax
	ret










;;;;;;;;;;;;;;;;;;
; rutinas que funcionan en modo 16 bit
;;;;;;;;;;;;;;;;;;
USE16



;;;;;;;; GDT EN ROM, esta esta "atornillada", se puede mover luego a RAM o definir otra
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
ALIGN 8
GDT16:
NULL_SEL	equ		$-GDT16
	dq		0x0						;descriptor nulo

CS_SEL   	equ 	$-GDT16			;descriptor de seg. codigo
	dw 		0xFFFF 					; limite
	dw 		0x0000					; base (16)
	db 		0x00					; base (8)

;	db 		0x9A
	db		10011010b
;			|||||||+---- Accedido = 0, el micro lo pone en 1 cada vez que se accede, a efectos de control por soft
;			||||||+----- Readable = 1 al ser de codigo significa que puede ser leido
;			|||||+------ Conforming = 0 significa segmento no conforme
;			||||+------- d/C = 1 significa segmento de Codigo 0 datos
;			|||+-------- No System = 1 (no es de sistema, sino  =0 es de sistema)
;			||+--------- \2 bits de DPL = 0 (mayor privilegio)
;			|+---------- /
;			+----------- Presente = 1 sino tira excepcion de segmento no presente
;
;	db 		0xCF					; attr: y limite (4)
	db		11001111b
;			|||||||+---- \
;			||||||+-----  \
;			|||||+------  / 4 bits de campo Limite
;			||||+------- /
;			|||+-------- Avl = 0 para uso por soft no impacta en el hard
;			||+--------- Long = para modo 64 bits
;			|+---------- Def = 1 es un segmento de codigo en 32 bits
;			+----------- Gran = 1 el campo Limite se interpreta como tama#o en paginas de 4k

	db 		0						; base (8)

DS_SEL 		equ 	$-GDT16			;descriptor de seg. datos
	dw 		0xFFFF 					; limite
	dw 		0x0000					; base
	db 		0x00					; base

;	db 		0x92					;
	db		10010010b
;			|||||||+---- Accedido = 0, el micro lo pone en 1 cada vez que se accede, a efectos de control por soft
;			||||||+----- Readable = 1 al ser de codigo significa que puede ser leido
;			|||||+------ Conforming = 0 significa segmento no conforme
;			||||+------- d/C = 0 significa segmento de datos
;			|||+-------- No System = 1 (no es de sistema, sino  =0 es de sistema)
;			||+--------- \2 bits de DPL = 0 (mayor privilegio)
;			|+---------- /
;			+----------- Presente = 1 sino tira excepcion de segmento no presente

	db 		0xCF					; idem al de codigo
	db 		0						; base

GDT_LENGTH 	equ 	$-GDT16

gdtr_img:
	dw 		GDT_LENGTH-1
	dd 		GDT16 + ROM_LOCATION






;;;;;;;;;;;;;;;;;;;;;;;
; marca fin de rutinas de inicio
;;;;;;;;;;;;;;;;;;;;;;;
times 0x10000-16-($-Inicio) db 0 	;relleno desde fin de rutina Inicio hasta vector de reset
FinProg equ $-Inicio				;







;;;;;;;;;;;;;;;;;;;;;;;
; vector de reset
;;;;;;;;;;;;;;;;;;;;;;;
Reset:
	cli
	jmp 	Inicio

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
times 16-($-Reset) db 0	; relleno desde vector reset hasta fin de ROM

Fin_codigo equ $-Inicio
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
