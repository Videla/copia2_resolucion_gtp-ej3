# Readme

Estructura de archivos:  

- __Makefile:__ comandos necesarios para construir el binario
- __linker.lds:__ script para el linker
- __bochs.cfg:__ configuración utilizada para el Bochs en cada ejercicio
- __init.asm:__ solo el código necesario para inicializar al procesador en modo protegido y en ejercicios posteriores la paginación.
- __main.asm:__ funcionalidad solicitada en cada ejercicio
- __functions.asm:__ funciones auxiliares y/o frecuentemente implementadas en ensamblador
- __functions.c:__ funciones auxiliares y/o frecuentemente implementadas en C
- __sys_tables.asm:__ tablas de sistema.
