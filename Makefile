AC		= nasm															# Assembler Compiler
AFLAGS		= -f bin # -felf32											# Assembler Flags
# CC		= gcc														# C Compiler
# CFLAGS		= -c -m32 -O0 -nostdlib 								# C Flags
LD		= ld															# linker LD
LFLAGS		= -z max-page-size=0x01000 --oformat=binary -m elf_i386		# Linker Flags
LFLAGS_2	= -z max-page-size=0x01000 -m elf_i386						# Linker Flags

LDSCRIPT	= linker.lds												# Linker Script

ODFLAGS		= -CprsSx --prefix-addresses

ROM_NAME	= mi_bios
ROM_LOCATION	= 0x000F0000

# OBJS		= rom-core.o init32.o auxlib32.o

.PHONY : all
all:
	# csg
	generate
	# @echo	''
	# @echo	'-----> Compilando...'
	# $(AC) -f bin main.asm -o $(ROM_NAME).bin -l main.lst
	# @echo	''


.PHONY : help
help:
	@echo	''
	@echo	'Uso:'
	@echo	'  make all:      Compila y patchea Checksum'
	@echo	'  make help:     Muestra estas opciones'
#	@echo	'  make generate: Compila y patchea Checksum'
	@echo	'  make dump:     Muestra archivo binario de imagen'
#	@echo	'  make edit:     Muestra todos los archivos para edicion'
	@echo	'  make bochs:    Ejecuta bochs con el archivo de configuracion de la version 2.5.1'
#	@echo	'  make csg:      Compila generador de checksum'
	@echo	'  make clean:    Elimina archivos auxiliares'
	@echo	''


.PHONY : generate
generate: $(ROM_NAME).bin $(ROM_NAME).elf	
	@echo	''
	@echo	'-----> Generando listados de hexadecimales y ELFs'
	hexdump -C $(ROM_NAME).bin > $(ROM_NAME)_hexdump.txt
	objdump $(ODFLAGS) $(ROM_NAME).elf > $(ROM_NAME)_objdump.txt
	readelf -a $(ROM_NAME).elf > $(ROM_NAME)_readelf.txt
	@echo	''
	
	# @echo	''
	# @echo	'-----> Generando Checksum'
	# ./CheckSumGen $(ROM_NAME).bin
	# @echo	'-----> Archivo patcheado'
	# @echo	''
	
$(ROM_NAME).bin: $(OBJS)
	@echo	''
	@echo	'-----> Linkeando objetos en formato binario'
	$(LD) $(LFLAGS) -o $@ $(OBJS) -T $(LDSCRIPT) -e Entry 
	
$(ROM_NAME).elf: $(OBJS)
	@echo	''
	@echo	'---> Linkeando objetos en formato ELF'
	$(LD) $(LFLAGS_2) -o $@ $(OBJS) -T $(LDSCRIPT) -e Entry 
	
# rom-core.o: rom-core.asm
# 	@echo	''
# 	@echo	'---> Compilando rom-core.asm'
# 	$(AC) $(AFLAGS) $< -o $@ -l rom-core.lst 

init.bin: init.asm
	@echo	''
	@echo	'---> Compilando init.asm'
	$(AC) -f bin $< -o $@ -l init.lst 

main.bin: main.asm
	@echo	''
	@echo	'---> Compilando main.asm'
	$(AC) -f bin main.asm -o $(ROM_NAME).bin -l main.lst



# init16.bin: init16.asm
# 	@echo	''
# 	@echo	'---> Compilando init16.asm'
# 	$(AC) -f bin $< -o $@ -l init16.lst 

# init32.o: init32.asm init16.bin
# 	@echo	''
# 	@echo	'---> Compilando init32.asm'
# 	$(AC) $(AFLAGS) $< -o $@ -l init32.lst 

# auxlib32.o: auxlib32.asm
# 	@echo	''
# 	@echo	'---> Compilando auxlib32.asm'
# 	$(AC) $(AFLAGS) $< -o $@ -l auxlib32.lst


.PHONY : dump
dump: 
	hexdump $(ROM_NAME).bin
#	hexdump $(ROM_NAME).img #En la version original exportaba a formato bin, en la de BochsLD lo hace a un .img.


# .PHONY : edit
# edit:
# 	geany *.asm *.c bochsrc Makefile  *.lst &
# 	kate *.asm bochsrc Makefile rom.lds *.txt *.lst &


.PHONY : bochs
bochs: 
	bochs -f bochsrc -q


# .PHONY : csg
# csg: 
# 	@echo	''
# 	@echo	'---> compilando generador de CheckSum'
# 	gcc CheckSumGen.c -o CheckSumGen


.PHONY : clean
clean:
	@echo	''
	@echo	'---> Limpieza de archivos'
	rm -f *.*~ *~ *.o *.lst *.bin *.rom *.txt *.elf *.img CheckSumGen